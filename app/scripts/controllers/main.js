'use strict';

angular.module('yoAppApp')
  .controller('MainCtrl', [ '$scope','$http', '$filter',  '$location','mainService', function ($scope, $http, $filter, $location, mainService) {

    // init

    $scope.topVideoCode = "xsCL_8nOy4o";
    $scope.topVideoTitle = "";

    $scope.languageSelected = function(choice){
        console.log("language selected=",choice);
        $scope.query.language = choice;
    }



    $scope.imageClicked = function(h)
    {
        console.log("carousel image clicked, h=",h);
        $scope.topVideoCode = h.vId;
        $scope.topVideoTitle = h.title;

    }

    // =======================================


    $scope.getThumbnailURL = function(h){
        if ( h==undefined || h==null)
            return null;
        var url = 'http://img.youtube.com/vi/'+ h.vId + '/1.jpg';
        //console.log("image tag=",url);
        return url;

    }

    $scope.gotoVideoList = function(){
        $location.path('/videolist');
    }

    $scope.getVideoList =function() {

        mainService.getVideoListAsync().then(
                function(data){
                    console.log("getVideoListAsync then data=",data);
                    console.log("mainService videoData = ", mainService.videoData);
                    $scope.videoList = mainService.videoData.videoList;
                    $scope.numberofVideos = $scope.videoList.length;
                    $scope.latestTenItems = $scope.videoList.slice(0,10);
                    $scope.topVideoCode = $scope.latestTenItems[0].vId;
                    $scope.topVideoTitle = $scope.latestTenItems[0].title
                },function(errorReason){
                    console.log("getVideoListAsync then error =",errorReason) ;
                }

        );


    };

    // ================================================
	$scope.getVideoList();

}]);


angular.module('yoAppApp')
    .directive('myYoutube', function($sce) {
        return {
            restrict: 'EA',
            scope: { code:'=' },
            replace: true,
            template: '<div style="height:400px;"><iframe style="overflow:hidden;height:100%;width:100%" width="100%" height="100%" src="{{url}}" frameborder="0" allowfullscreen></iframe></div>',
            link: function (scope) {
                console.log('here');
                scope.$watch('code', function (newVal) {
                    if (newVal) {
                        console.log('myYoutube, code changed = ',newVal);
                        scope.url = $sce.trustAsResourceUrl("//www.youtube.com/embed/" + newVal);
                    }
                });
            }
        };
    });

