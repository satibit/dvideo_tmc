angular.module('yoAppApp')
    .service('mainService', function($http) {
        var promise;
        //shared between controllers
        var rawVideoData = {
                videoList:[]
        };
        return{
            videoData: rawVideoData, // pass a reference
            numberofVideos: rawVideoData.videoList.length,
            getVideoListAsync: function(){
                if ( !promise )

                    var nocache= moment().format('YYYYMMDDHH');
                    promise = $http.get('testdata/videodata.json?cache=' + nocache).then(function(ret){
                        rawVideoData.videoList = ret.data;
                        console.log("inside mainService. $http.get.then success data=",ret.data);
                        return ret.data;
                    });
                return promise;
            }
        };
    });

