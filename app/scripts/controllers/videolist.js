'use strict';

angular.module('yoAppApp')
    .controller('VideoListCtrl', [ '$scope','$http', '$filter', 'mainService', function ($scope, $http, $filter, mainService) {

        $scope.sortingOrder = "date";
        $scope.reverse = true;
        $scope.filteredItems = [];
        $scope.groupedItems = [];
        $scope.itemsPerPage = 5;
        $scope.pagedItems = [];
        $scope.currentPage = 1; // start from 1 to match with pagination
        $scope.maxPaginationSize = 5;
        $scope.query = "";

        $scope.myInterval = 500000;

        $scope.languages= [
            "All Languages",
            "English",
            "Vietnamese",
            "Burmese"
        ];
        $scope.selectedLanguage = 'All Languages';

        $scope.getThumbnailURL = function(h){
            if ( h==undefined || h==null)
                return null;
            var url = 'http://img.youtube.com/vi/'+ h.vId + '/1.jpg';
            //console.log("image tag=",url);
            return url;

        }

        $scope.getLinkCode = function(h){
            var link = 'http://www.youtube.com/watch?v=' + h.vId;
            return link;
        }


        var searchMatch = function (haystack, needle) {
            if (!needle) {
                return true;
            }
            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
        };

        // init the filtered items
        $scope.search = function () {
            $scope.filteredItems = $filter('filter')($scope.videoList, function (item) {
                // check the language first
                if ( $scope.selectedLanguage != 'All Languages' &&
                     item['language'] != $scope.selectedLanguage )
                    return false;

                for(var attr in item) {
                    if (searchMatch(item[attr].toString(), $scope.query))
                    {
                        //console.log("searchMatch !  return now!");
                        return true;
                    }
                }
                return false;
            });
            // take care of the sorting order
            if ($scope.sortingOrder !== '') {
                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
            }

            //console.log("filtedItems=", $scope.filteredItems);
            $scope.currentPage = 1;
            // now group by pages
            $scope.groupToPages();
        };

        // calculate page in place
        $scope.groupToPages = function () {
            $scope.pagedItems = [];

            for (var i = 0; i < $scope.filteredItems.length; i++) {
                if (i % $scope.itemsPerPage === 0) {
                    //console.log("new page, first item=", $scope.filteredItems[i].title);
                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                } else {
                    //console.log("adding items to current page " + Math.floor(i / $scope.itemsPerPage) + "  " + $scope.filteredItems[i].title);
                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                }
            }
        };

        $scope.range = function (start, end) {
            var ret = [];
            if (!end) {
                end = start;
                start = 0;
            }
            for (var i = start; i < end; i++) {
                ret.push(i);
            }
            return ret;
        };

        // change sorting order
        $scope.sort_by = function(newSortingOrder) {
            if ($scope.sortingOrder == newSortingOrder)
                $scope.reverse = !$scope.reverse;

            $scope.sortingOrder = newSortingOrder;

            // icon setup
            $('th a i').each(function(){
                // icon reset
                $(this).removeClass().addClass('icon-sort');
            });
            if ($scope.reverse)
                $('th.'+newSortingOrder+' i').removeClass().addClass('icon-chevron-up');
            else
                $('th.'+newSortingOrder+' i').removeClass().addClass('icon-chevron-down');
        };

        $scope.prevPage = function () {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
            }
        };

        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.pagedItems.length) {
                $scope.currentPage++;
            }
        };

        $scope.setPage = function () {
            $scope.currentPage = this.n + 1;
        };

        $scope.getVideoList =function() {

            mainService.getVideoListAsync().then(
                function(data){
                    console.log("getVideoListAsync then data=",data);
                    console.log("mainService videoData = ", mainService.videoData);
                    $scope.videoList = mainService.videoData.videoList;
                    $scope.numberofVideos = $scope.videoList.length;
                    $scope.latestTenItems = $scope.videoList.slice(0,10);
                    $scope.topVideoCode = $scope.latestTenItems[0].vId;
                    $scope.topVideoTitle = $scope.latestTenItems[0].title
                    $scope.search();
                },function(errorReason){
                    console.log("getVideoListAsync then error =",errorReason) ;
                }

            );
        };

        $scope.languageSelected = function(choice){
            console.log("language changed to: ", choice);
            $scope.selectedLanguage = choice;
            $scope.search();

        }


        // ================================================
        $scope.getVideoList();


    }]);
