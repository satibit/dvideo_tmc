'use strict';


angular.module('yoAppApp', ['ngRoute','ui.bootstrap', 'ngAnimate', 'angular-flexslider'])
  .config(['$routeProvider',function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/videolist',{
        templateUrl: 'views/videolist.html',
        controller: 'VideoListCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
